# LIS 4368	

## Gatlin Lowe

### Project #2 Requirements:

1. Added read, add, updated, and delete function to website
2. Added a display database function to website

#### README.md file should include the following items:

* Screenshot of Validated Form

* Screenshot of Passed Validation Form

* Screenshot of Display Database Form

* Screenshot of Modify Form

* Screenshot of Passed Modified Form

* Screenshot of Delete Message

* Screenshot of Database after Adding, Updating, and Deleting


#### Assignment Screenshots:

*Screenshot of Validated Form*:

![Validated Screenshot](img/validform.png)

*Screenshot of Passed Validation Form*:

![Passed Validation Screenshot](img/passedvalidation.png)

*Screenshot of Displayed Database Form*:

![Display Screenshot](img/display.png)

*Screenshot of Modify Form*:

![Modify Screenshot](img/modify.png)

*Screenshot of Passed Modify Form*:

![Passed Modify Screenshot](img/passedmodify.png)

*Screenshot of Delete Message*:

![Delete Screenshot](img/delete.png)

*Screenshot of Database*:

![Database Screenshot](img/db.png)