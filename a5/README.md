> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368	

## Gatlin Lowe

### Assignment #5 Requirements:

*Sub-Heading:*

1. Provide Java files to connect Server and MySQL
2. Code program to watch for SQL Injection
3. Complile Java files correctly



#### Assignment Screenshots:

*Screenshot of Valid Form Entry running*:

![Valid Form Entry Screenshot](img/validform.png)

*Screenshot of Passed Validation*:

![Passed Validation](img/passvalid.png)

*Screenshot of Associated Database Entry*:

![MySQL Screenshot](img/mysql.png)

*Screenshot of LargestOfThreeNumbers.java*:

![LargestOfThreeNumbers Screenshot](img/largestthreenumbers.png)

*Screenshot of SimpleCalculate.java*:

![SimpleCalculator Screenshot](img/SimpleCalc.png)

*Screenshot of NumberSwap.java*:

![NumberSwap Screenshot](img/NumberSwap.png)


