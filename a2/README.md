# LIS 4368	

## Gatlin Lowe

### Assignment #2 Requirements:

*Deliverables:*

1. Downlad and Installed AMPPS and MySQL
2. Login to AMPPS through MySQL
3. Add database into MySQL and test through the website
4. Finish Tomcat tutorial on website and edit the HTML files
5. Test the website files for both Tomcat and MySQL/AMPPS

#### README.md file should include the following items:

* Screenshot of index.html from hello folder
* Screenshot of sayHello
* Screenshot of Querybook
* Screenshot of results from Querybook



### Assignment Screenshots:

*Screenshot of Before index http://localhost/lis4368/a2/*:

![index Screenshot](img/a2_ss.png)

*Screenshot of Before index http://localhost/hello/*:

![index Screenshot](img/hello_page.png)

*Screenshot of index http://localhost/hello/*:

![index Screenshot](img/index.png)

*Screenshot of sayHello webpage http://localhost/hello/sayhello/:*

![sayhello Screenshot](img/sayhello.png)

*Screenshot of sayHi webpage http://localhost/hello/sayhi/:*

![sayhello Screenshot](img/sayhi.png)

*Screenshot of Querybook http://localhost/querybook.html/*

![index Screenshot](img/querybook.png)

*Screenshot of Querybook with selection http://localhost/querybook.html*:

![index Screenshot](img/querybook_selected.png)





*Screenshot of Querybook with results http://localhost/querybook.html/*:

![index Screenshot](img/queryresults.png)

#### Tutorial Links:

*Assignment 1 Location:*
[A1 Bitbucket Link](https://bitbucket.org/grl18/lis4368/src/master/a1/ "A1 location")


