<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sat, 01-02-21, 19:38:14 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Gatlin Lowe">
	<link rel="icon" href="img/favicon.ico">

	<title>LIS4368 - Assignment 2</title>

	<%@ include file="/css/include_css.jsp" %>
	<style type="text/css">
	.navbar-inverse {
		background-image: linear-gradient(to bottom,#4b8c43 0,#1c3619 100%);
	}
	.navbar-inverse .navbar-nav>li>a {
			color: #fff;
	}
	.navbar-inverse .navbar-brand {
			color: #fff;
	}
	</style>
</head>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>

<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="/global/nav.jsp" %>

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<%@ include file="global/header.jsp" %>
					</div>

					<b>Servlet Compilation and Usage:</b><br />
					<img src="img/sayhello.png" class="img-responsive center-block" alt="Using sayhello Servlet" />
					<img src="img/sayhi.png" class="img-responsive center-block" alt="Using sayhi Servlet" />
					<img src="img/querybook.png" class="img-responsive center-block" alt="Using Querybook Servlet" />

					<br /> <br />
					<b>Database Connectivity Using Servlets:</b><br />
					<img src="img/querybook_selected.png" class="img-responsive center-block" alt="Database Connectivity Querybook Servlet" />
				<br />
					<img src="img/queryresults.png" class="img-responsive center-block" alt="Querybook results" />

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>

</body>
</html>
