import java.util.Scanner;

public class ASCII
{
    public static void main (String [] args)
    {
        int num = 0;
        boolean isValidNum = false;
        Scanner j = new Scanner(System.in);

        System.out.println("Developer: Gatlin Lowe" + "\n" + "Program to print characeters A-Z as ASCII values: ");

        for (char character = 'A'; character <= 'Z'; character++) {
            
            System.out.printf("Character %c has ascii value %d\n", character, ((int)character));
        }

        System.out.println("\nPrinting ASCII values 48-122 as characters:");

        for(num=48; num <= 122; num++) {

            System.out.printf("ASCII value %d has character value %c \n", num, ((char)num));
        }

            System.out.println("Please enter ASCII value (32-127): ");
            if(j.hasNextInt()){
                
                num = j.nextInt();
                isValidNum = true;
            
            } else {
               
                System.out.println("Invalid integer --  ASCII value must be a number. \n");
            
            }
            
            j.nextLine();

            if(isValidNum = true && num < 32 || num > 127) {

                System.out.println("ASCII value must be >=32 and <= 127. \n");
                isValidNum = false;

            }

            if (isValidNum = true) {

                System.out.println();
                System.out.printf("ASCII value %d has character value %c \n", num, ((char)num));

            }







    }
}
