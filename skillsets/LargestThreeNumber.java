import java.util.Scanner;

public class LargestThreeNumber 
{
    public static void main (String [] args) 
    {

        System.out.println("Program evaluates the largest of three numbers.\n");
        
        int num1, num2, num3;
        Scanner j = new Scanner(System.in);


        System.out.println("Please enter first number: ");


        while(!j.hasNextInt()) {
            System.out.println("Not a valid integer!");
            j.next();
            System.out.println("Please try again. Enter first number: ");
        }

        num1 = j.nextInt();
        System.out.println("Please enter second number: ");

        while(!j.hasNextInt()) {
            System.out.println("Not a valid integer!");
            j.next();
            System.out.println("Please try again. Enter second number: ");
        }

        num2 = j.nextInt();
        System.out.println("Please enter third number: ");

        while(!j.hasNextInt()) {
            System.out.println("Not a valid integer!");
            j.next();
            System.out.println("Please try again. Enter third number: ");
        }

        num3 = j.nextInt();

        System.out.println();

        if (num1 > num2 && num1 > num3) {
            System.out.println("The first number is the largest! (" + num1 + ")");
        } else if (num2 > num1 && num2 > num3) {
            System.out.println("The second number is the largest! (" + num2 + ")"); 
        } else if (num3 > num1 && num3 > num2) {
            System.out.println("The third number is the largest! (" + num3 + ")");
        } else {
            System.out.println("Two or more numbers where the same and the largest!");
        }

    }
}