import java.text.DecimalFormat;
import java.util.Scanner;
class GradeApp {
	public static void main (String args[]){
		
    Scanner grades = new Scanner(System.in);
    double totalgrade = 0;
    double adding = 0;
    double enteredgrade = 0;
    int gradecount = 0;
    double averagegrade;
    
		
    System.out.println("Please enter grades that range from 1 to 100.");
  	System.out.println("Grade average and total is rounded to 2 decimal places. \nNote: Program does *not* check for non-numeric characters. \nTo end program enter: -1");
  	System.out.println();
      
     while (enteredgrade != -1) {
      System.out.print("Enter grade:");
      enteredgrade = grades.nextDouble();
          
          if (enteredgrade >= 0 && enteredgrade < 101){
             
            adding = enteredgrade;
            totalgrade = adding + totalgrade;
            gradecount++;

          }
          else if(enteredgrade >100 && enteredgrade <-1) 
            System.out.println("Invalid Entry, enter not counted");
          
          
          
          
     }
        if (gradecount == 0 ) {
            averagegrade = 0;

        } else {
          averagegrade = totalgrade / gradecount;
        }

        DecimalFormat f = new DecimalFormat("##.00");

        String messege = "\n" + 
        "Grade count:" + gradecount + "\n" + 
        "Grade total:" + f.format(totalgrade) + "\n" +
        "Average grade:" + f.format(averagegrade) + "\n";
        System.out.println(messege);
    
    

    	
    
    // int inum = Integer.parseInt(str);
    
    
    
  }//end main
}//end class
