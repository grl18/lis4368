import java.util.Scanner;

public class SimpleCalc {

	public static void main (String args[]) {
  
  Scanner input = new Scanner(System.in);
  double num1 = 0.0;
  double num2 = 0.0;
  String j;
  
  System.out.println("Enter mathematical operations (a = add, s = subtract, m = multiply, d = divide, p = power) : ");
  j = input.next().toLowerCase();
  
  while (!j.equals("a") && !j.equals("s") && !j.equals("m") && !j.equals("d") && !j.equals("p")) {
  
  	System.out.println("Incorrect operation! Please enter correct opperation (a,s,m,d,p): ");
    j = input.next().toLowerCase();

  }
  
    System.out.println("\nPlease enter the first number: ");

  while(!input.hasNextDouble()) {

      System.out.println("Not a valid number!\n");
      input.next();
      System.out.println("Please try again. Enter first number: ");
  }
  num1 = input.nextDouble();

  System.out.println("Please enter second number: ");

  while(!input.hasNextDouble()) {

    System.out.println("Not a valid number!\n");
    input.next();
    System.out.println("Please try again. Enter second number: ");
  }

  num2 = input.nextDouble();

    if(j.equals("a")){
      Add(num1, num2);
    } else if (j.equals("s")) {
      Subtract(num1, num2);
    } else if (j.equals("m")) {
      Multiply(num1, num2);
    } else if (j.equals("d")) {
      if(num2 == 0) {
        System.out.println("Cannout divide by zero");
      } else {
      Divide(num1, num2);
      }
    } else if (j.equals("p")) {
      Power(num1, num2);
    }

  }
  
  public static void Add(double n1, double n2) 
  {
  System.out.println(n1 + "+" + n2 + "=");
  System.out.printf("%.2f",(n1 + n2));
  }
  
  public static void Subtract(double n1, double n2) 
  {
  System.out.println(n1 + "-" + n2 + "=");
  System.out.printf("%.2f",(n1 - n2));
  }

  public static void Multiply(double n1, double n2) 
  {
  System.out.println(n1 + "*" + n2 + "=");
  System.out.printf("%.2f",(n1 * n2));
  }

  public static void Divide(double n1, double n2) 
  {
  System.out.println(n1 + "/" + n2 + "=");
  System.out.printf("%.2f",(n1 / n2));
  }
  
  public static void Power(double n1, double n2) 
  {
  System.out.println(n1 + "to the power of" + n2 + "=");
  System.out.printf("%.2f",(Math.pow(n1,n2)));
  }

}