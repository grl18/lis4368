import java.util.Scanner;

 public class NumberSwap {
  public static void main(String args[]) 
  {
    System.out.println("Program swaps two integers.");
    System.out.println();

    int num1, num2, temp; 
    Scanner sc = new Scanner(System.in);


  
        System.out.println("Please enter first number: ");

         while (!sc.hasNextInt()) 
         {
          System.out.println("Not valid integer!");
          sc.next();
          System.out.print("Please try again. Enter first number: ");
         } 
         num1 = sc.nextInt();

        System.out.println("Please enter second number: ");
          
        while (!sc.hasNextInt()){
            System.out.println("Not valid integer!");
            System.out.print("Please try again. Enter second number: ");
            sc.next();
          }

          num2 = sc.nextInt();
    
        System.out.println("Before Swapping: ");
        System.out.println("num1 = " + num1);
        System.out.println("num2 = " + num2);

 

        temp = num1;
        num1 = num2;
        num2 = temp;
        

          System.out.println("After Swapping: ");
          System.out.println("num1 = " + num1);
          System.out.println("num2 = " + num2); 

  }

}


