> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368	

## Gatlin Lowe

### Assignment # Requirements:

*Sub-Heading:*

1. Provide server-side validation 
2. Compile Customer.java and CustomerServlet.java
3. Java 10 CopyArray
4. Java 11 FileWriteReadCountWords
5. Java 12 ASCII

#### README.md file should include the following items:

* Screenshot of invalidated webpage
* Screenshot of working webpage



#### Assignment Screenshots:

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/failed.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passed.png)

