> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4368 Advanced Web App Development

## Gatlin Lowe

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")

    - Installed Tomcat

    - Installed JDK

    - Installed Atom

    - Installed SourceTree



2. [A2 README.md](a2/README.md "My A2 README.md file")
- Installed AMPPS
    - Installed MySQL

    - Linked AMPPS and MySQL

    - Tested the database to work with the website

    - Created and Tested Servlets


3. [A3 README.md](a3/README.md "My A3 README file")

    - Connect to local database using AMPPS

    - Create local database with MySQL

    - Include 10 sets of data within three tables


3. [P1 README.md](p1/README.md "My Project 1 README file")

    - Added JQuery validation to p1/index.jsp

    - Set min and max to the validation

    - Test and fix the webpage to work

4. [A4 README.md](a4/README.md "My A4 README file")

    - Provide server-side validation 

    - Compile Customer.java and CustomerServlet.java

5. [A5 README.md](a5/README.md "My A5 README file")
    
    - Provide Java files to connect Server and MySQL

    - Code program to watch for SQL Injection

    - Complile Java files correctly

6. [P2 README.md](p2/README.md "My P2 README file")

    - Added read, add, updated, and delete function to website

    - Added a display database function to website

7. Skill Sets

    - Java 10 CopyCharacters

    - Java 11 FileWriteReadCountWords

    - Java 12 ASCII

    - Java 13 NumberSwap

    - Java 14 LargestOfThreeNumbers

    - Java 15 SimpleCalculator