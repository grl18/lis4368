# LIS 4368	

## Gatlin Lowe

### Assignment #3 Requirements:

*Deliverables:*

1. Connect to local database using AMPPS
2. Create local database with MySQL
3. Include 10 sets of data within three tables

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of a3/index.jsp
* Links to a3.sql & a3.mwb

  

#### Assignment Screenshots:

*Screenshot of A3 ERD :*

![A3 ERD Screenshot](img/erd.png)



*Screenshot of A3 page*:

![A3 Index Screenshot](img/a3_ss.png)

*Link to a3.mwb:*

[a3.mwb file](docs/a3.mwb)

*Link to a3.sql:*

[a3.sql file](docs/a3.sql)


#### Tutorial Links:

*A1- Station Location:*
[A1 Bitbucket Locations Link](https://bitbucket.org/grl18/lis4368/src/master/a1/ "A1 Bitbucket Location")

*A2 - Station Location:*
[A2 Bitbucket Locations Link](https://bitbucket.org/grl18/lis4368/src/master/a2/ "A2 Bitbucket Location")