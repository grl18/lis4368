<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sat, 01-02-21, 19:40:00 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Gatlin Lowe">
	<link rel="icon" href="img/favicon.ico">

	<title>LIS4368 - Assignment1</title>

	<%@ include file="/css/include_css.jsp" %>
	<style type="text/css">
	.navbar-inverse {
		background-image: linear-gradient(to bottom,#4b8c43 0,#1c3619 100%);
	}
	.navbar-inverse .navbar-nav>li>a {
			color: #fff;
	}
	.navbar-inverse .navbar-brand {
			color: #fff;
	}
	</style>
</head>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>

<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="/global/nav.jsp" %>

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<%@ include file="global/header.jsp" %>
					</div>

					<b>Petstore Database (Entity Relationship Diagram):</b><br />
					<img src="img/erd.png" class="img-responsive center-block" alt="A3 ERD" />

					<br /> <br />
					<h2><b>MySQL Workbench and SQL Files:</b></h2><br />
				<h4><a href="docs/a3.mwb">Petstore MySQL Workbench File</a></h4>
				<br />
					<h4><a href="docs/a3.sql">Petstore SQL File</a></h4>
					<br><br>

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>

</body>
</html>
