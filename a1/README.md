# LIS 4368	

## Gatlin Lowe

### Assignment #1 Requirements:

*Three Parts:*

1. Distrubuted Version Control with Git and Bitbucket
2. Java/JSP/Serverlet Development Installation
3. Chapter Questions (Ch.1 -  4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 Above)
* Screenshot of running http://localhost:9999 (#2 Above)
* Git commands with descriptions
* Bitbucketrepo links: A) This assignment B)The completed tutorial above (bitbucketstationlocation)



> #### Git commands w/short descriptions:

1. git init - create an empty Git repository or reinitialize an existing one
2. git status - show the working tree status
3. git add - add file contents to the index
4. git commit - record changes to the repository 
5. git push - update remote refs along with associated objects
6. git pull - fetch from and integrate with another repository or a local branch
7. git log - show commit logs

#### Assignment Screenshots:

*Screenshot of Tomcat running http://localhost:9999*

![tomcat_install](img/tomcat_install.png)



*Screenshot of running java Hello*:



![Hello_Java](img/Hello_Java.png)

*Screenshot of JDK install:*



![JDK Installation Screenshot](img/jdk_install.png)

*A1 Images working Screenshot*

![img_working](img/img_working.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[ Bitbucket Station Locations Tutorial Link](https://bitbucket.org/grl18/bitbucketstationlocations/ "Bitbucket Station Locations")

