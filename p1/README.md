# LIS 4368	

## Gatlin Lowe

### Project #1 Requirements:

1. Add JQuery validation to p1/index.jsp
2. Set min and max to the validation
3. Test and fix the webpage to work

#### README.md file should include the following items:

* Screenshot of nonvalidated webpage

* Screenshot of validated webpage

  

#### Assignment Screenshots:

*Screenshot of nonvalidated webpage*:

![Nonvalidated Screenshot](img/nonvalidated.png)

*Screenshot of Validated Webpage*:

![Validated Screenshot](img/validated.png)



